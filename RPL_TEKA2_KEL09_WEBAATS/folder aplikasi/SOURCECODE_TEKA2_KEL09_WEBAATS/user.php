<?php include ('header.php');?>

  

            <div class="clearfix"></div>

            <div class="">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-bars"></i> DATA <small>Pengguna</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                       
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#siswa"  role="tab" data-toggle="tab" aria-expanded="true">siswa</a>
                        </li>
                        <li role="presentation" class=""><a href="#Guru" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Guru</a>
                        </li>
                        <li role="presentation" class=""><a href="#ortu" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Ortu Siswa</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="siswa" >
                     <div class="x_content">
                      <center><a href="#addsiswa" data-toggle="modal"><button type='button' class='btn btn-success btn-sm'><span class='glyphicon glyphicon-plus' aria-hidden='true'> Tambah Pengguna Siswa </span></button></a></center>
                    <table id="datatable" class="table table-striped table-bordered" >
                      <thead>
                        <tr>
                            <th>  No </th>
                            <th> username </th>
                            <th> Akses </th>
                            <th>  Aksi </th>
                        </tr>
                      </thead>
                      <tbody>
                       
                     <?php 
                  
                    $x =1;
                    $sql = "SELECT `username`, `password`, `id_ortu`, `nis`, `id_admin`, `nip`, `akses`, `waktu_daftar` FROM `user` WHERE akses = 'siswa' ";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $id = $row['username'];
                            $nis = $row['nis'];
                            $ortu = $row['id_ortu'];
                            $nip = $row['nip'];
                            $admin = $row['id_admin'];
                            $akses = $row['akses'];
                       echo "
                    <tr>
      
                      <td><center>$x</center></td>
                      <td><center>$id</center></td>
                      <td><center>$akses</center></td>

                       "; 
                    $x++; ?>
                     <td> <center>
                            <a href="#edit<?php echo $id;?>" data-toggle="modal"><button type='button' class='btn btn-warning btn-sm gfa-edit '><span class='glyphicon glyphicon-edit' aria-hidden='true'>Ubah</span></button></a>
                            <a href="#delete<?php echo $id;?>" data-toggle="modal"><button type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-trash' aria-hidden='true'> Hapus</span></button></a>
                     </center>
                    </td>
                  </tr>
                   <!--add Item Modal -->
                    <div id="addsiswa" class="modal fade" role="dialog">
                        <form method="post" action="t_user_siswa.php">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <h4 class="modal-title">Tambah Pengguna Siswa</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       
                                    </div>
                                    <div class="modal-body">
                                      <div class="form-group">
                                              </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="id">Username</label>
                                                <input type="text" class="form-control" id="username" name="username" aceholder=" ID ortu" >
                                              </div>
                                              <div class="form-group">
                                            <label class="control-label col-sm-3" for="password">Password</label>
                                           <input type="password" class="form-control" id="password" name="pas" laceholder="Nama ortu" >
                                              </div>
                                              <div class="form-group">
                                            <label class="control-label col-sm-3" for="nis">NIS Siswa</label>
                                           <input type="text" class="form-control" id="nis" name="nis" laceholder="NIS Siswa" >
                                           <input type="hidden" class="form-control" id="akses" name="akses" value="siswa" laceholder="NIS Siswa" >
                                              </div>
                                             
                                    <div class="modal-footer">
                                        <button type="submit"  name="tambah" class="btn btn-primary"><span class="glyphicon glyphicon-tambah"></span> tambah</button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </form>
                    </div>

                  
                  <!--Edit Item Modal -->
        <div id="edit<?php echo $id; ?>" class="modal fade" role="dialog">
            <form method="post" action="u_siswa.php" >
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Pembaharuan Pengguna Siswa</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         
                        </div>
                        <div class="modal-body">
          
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">NIS Siswa</label>
                                    <input type="number" class="form-control" id="nis" name="nis" value="<?=$nis?>" placeholder="NIS" readonly>
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="username">Username </label>
                               <input type="text" class="form-control" id="username" name="username" value="<?=$id?>" placeholder="username" >
                                  </div>
                                  
                        <div class="modal-footer">
                            <button type="submit"  name="ubah" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span> Ubah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
                  <!-- delete Unit  Modal-->
     <div id="delete<?php echo $id; ?>" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form method="post">
                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">
                           <h4 class="modal-title">Delete</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                        </div>

                        <div class="modal-body">
                            <input type="hidden" name="delete_id" value="<?php echo $id; ?>">
                            <p>
                                <div class="alert alert-danger">Apakah kamu yakin Mau Menghapus <strong><?php echo $id; ?>?</strong></p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="delete" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YA</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> TIDAK</button>
                            </div>
                        </div>
                </form>
                </div>
            </div>s
                  <?php  } }?>
                      </tbody>
                    </table>
                  


                  </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="Guru" >
                              <div class="x_content">
                      <center><a href="#addguru" data-toggle="modal"><button type='button' class='btn btn-success btn-sm'><span class='glyphicon glyphicon-plus' aria-hidden='true'> Tambah Pengguna Guru</span></button></a></center>
                    <table id="datatable" class="table table-striped table-bordered" >
                      <thead>
                        <tr>
                            <th>  No </th>
                            <th> username </th>
                            <th> Akses </th>
                            <th>  Aksi </th>
                        </tr>
                      </thead>
                      <tbody>
                       
                     <?php 
                  
                    $x =1;
                    $sql = "SELECT `username`, `password`, `id_ortu`, `nis`, `id_admin`, `nip`, `akses`, `waktu_daftar` FROM `user` WHERE akses like 'guru' ";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $id = $row['username'];
                            $nis = $row['nis'];
                            $ortu = $row['id_ortu'];
                            $nip = $row['nip'];
                            $admin = $row['id_admin'];
                            $akses = $row['akses'];
                       echo "
                    <tr>
      
                      <td><center>$x</center></td>
                      <td><center>$id</center></td>
                      <td><center>$akses</center></td>

                       "; 
                    $x++; ?>
                     <td> <center>
                            <a href="#edit<?php echo $id;?>" data-toggle="modal"><button type='button' class='btn btn-warning btn-sm gfa-edit '><span class='glyphicon glyphicon-edit' aria-hidden='true'>Ubah</span></button></a>
                            <a href="#delete<?php echo $id;?>" data-toggle="modal"><button type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-trash' aria-hidden='true'> Hapus</span></button></a>
                     </center>
                    </td>
                  </tr>

                  <!--add Item Modal -->
                    <div id="addguru" class="modal fade" role="dialog">
                        <form method="post" action="t_user_guru.php">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <h4 class="modal-title">Tambah Pengguna Guru</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       
                                    </div>
                                    <div class="modal-body">
                                      <div class="form-group">
                                              </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="id">Username</label>
                                                <input type="text" class="form-control" id="username" name="username" aceholder=" ID ortu" >
                                              </div>
                                              <div class="form-group">
                                            <label class="control-label col-sm-3" for="password">Password</label>
                                           <input type="password" class="form-control" id="password" name="pas" laceholder="Nama ortu" >
                                              </div>
                                              <div class="form-group">
                                            <label class="control-label col-sm-3" for="nip">NIP GURU</label>
                                           <input type="text" class="form-control" id="nip" name="nip" laceholder="NIP Guru " >
                                           <input type="hidden" class="form-control" id="akses" name="akses" value="guru" laceholder="NIP Guru" >
                                              </div>
                                             
                                    <div class="modal-footer">
                                        <button type="submit"  name="tambah" class="btn btn-primary"><span class="glyphicon glyphicon-tambah"></span> tambah</button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </form>
                    </div>
                  <!--Edit Item Modal -->
        <div id="edit<?php echo $id; ?>" class="modal fade" role="dialog">
            <form method="post" action="u_ortu.php" >
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Pembaharuan Data Jurusan</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <!--  // SELECT `id_ortu`, `nama`, `kd_nis`, `alamat`, `no_hp`, `foto`, `jk`, `email` FROM `orang_tua` WHERE 1 -->
                        </div>
                        <div class="modal-body">
          
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">Id</label>
                                    <input type="number" class="form-control" id="editortu" name="editortu" value="<?php echo $id; ?>" placeholder="NIS" readonly>
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="editnama">Nama Jurusan </label>
                               <input type="text" class="form-control" id="editnama" name="editnama" value="<?php echo $jurusan; ?>" placeholder="Nama ortu" >
                                  </div>
                                  
                        <div class="modal-footer">
                            <button type="submit"  name="ubah" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span> Ubah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
                  <!-- delete Unit  Modal-->
     <div id="delete<?php echo $id; ?>" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form method="post">
                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">
                           <h4 class="modal-title">Delete</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                        </div>

                        <div class="modal-body">
                            <input type="hidden" name="delete_id" value="<?php echo $id; ?>">
                            <p>
                                <div class="alert alert-danger">Apakah kamu yakin Mau Menghapus <strong><?php echo $id; ?>?</strong></p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="delete" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YA</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> TIDAK</button>
                            </div>
                        </div>
                </form>
                </div>
            </div>s
                  <?php  } }?>
                      </tbody>
                    </table>
                    


                  </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="ortu" >
                              <div class="x_content">
                      <center><a href="#addortu" data-toggle="modal"><button type='button' class='btn btn-success btn-sm'><span class='glyphicon glyphicon-plus' aria-hidden='true'> Tambah Pengguna Ortu </span></button></a></center>
                    <table id="datatable" class="table table-striped table-bordered" >
                      <thead>
                        <tr>
                            <th>  No </th>
                            <th> username </th>
                            <th> Akses </th>
                            <th>  Aksi </th>
                        </tr>
                      </thead>
                      <tbody>
                       
                     <?php 
                  
                    $x =1;
                    $sql = "SELECT `username`, `password`, `id_ortu`, `nis`, `id_admin`, `nip`, `akses`, `waktu_daftar` FROM `user` WHERE akses like 'ortu' ";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $id = $row['username'];
                            $nis = $row['nis'];
                            $ortu = $row['id_ortu'];
                            $nip = $row['nip'];
                            $admin = $row['id_admin'];
                            $akses = $row['akses'];
                       echo "
                    <tr>
      
                      <td><center>$x</center></td>
                      <td><center>$id</center></td>
                      <td><center>$akses</center></td>

                       "; 
                    $x++; ?>
                     <td> <center>
                            <a href="#edit<?php echo $id;?>" data-toggle="modal"><button type='button' class='btn btn-warning btn-sm gfa-edit '><span class='glyphicon glyphicon-edit' aria-hidden='true'>Ubah</span></button></a>
                            <a href="#delete<?php echo $id;?>" data-toggle="modal"><button type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-trash' aria-hidden='true'> Hapus</span></button></a>
                     </center>
                    </td>
                  </tr>

                     <!--add Item Modal -->
                    <div id="addortu" class="modal fade" role="dialog">
                        <form method="post" action="t_user_guru.php">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <h4 class="modal-title">Tambah Pengguna Ortu</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       
                                    </div>
                                    <div class="modal-body">
                                      <div class="form-group">
                                              </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="id">Username</label>
                                                <input type="text" class="form-control" id="username" name="username" aceholder=" ID ortu" >
                                              </div>
                                              <div class="form-group">
                                            <label class="control-label col-sm-3" for="password">Password</label>
                                           <input type="password" class="form-control" id="password" name="pas" laceholder="Nama ortu" >
                                              </div>
                                              <div class="form-group">
                                            <label class="control-label col-sm-3" for="id_ortu">ID Ortu</label>
                                           <input type="text" class="form-control" id="id_ortu" name="id_ortu" laceholder="id Ortu " >
                                           <input type="hidden" class="form-control" id="akses" name="akses" value="ortu" laceholder="id Ortu" >
                                              </div>
                                             
                                    <div class="modal-footer">
                                        <button type="submit"  name="tambah" class="btn btn-primary"><span class="glyphicon glyphicon-tambah"></span> tambah</button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </form>
                    </div>
                  <!--Edit Item Modal -->
        <div id="edit<?php echo $id; ?>" class="modal fade" role="dialog">
            <form method="post" action="u_ortu.php" >
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Pembaharuan Data Jurusan</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <!--  // SELECT `id_ortu`, `nama`, `kd_nis`, `alamat`, `no_hp`, `foto`, `jk`, `email` FROM `orang_tua` WHERE 1 -->
                        </div>
                        <div class="modal-body">
          
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">Id</label>
                                    <input type="number" class="form-control" id="editortu" name="editortu" value="<?php echo $id; ?>" placeholder="NIS" readonly>
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="editnama">Nama Jurusan </label>
                               <input type="text" class="form-control" id="editnama" name="editnama" value="<?php echo $jurusan; ?>" placeholder="Nama ortu" >
                                  </div>
                                  
                        <div class="modal-footer">
                            <button type="submit"  name="ubah" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span> Ubah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
                  <!-- delete Unit  Modal-->
     <div id="delete<?php echo $id; ?>" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form method="post">
                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">
                           <h4 class="modal-title">Delete</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                        </div>

                        <div class="modal-body">
                            <input type="hidden" name="delete_id" value="<?php echo $id; ?>">
                            <p>
                                <div class="alert alert-danger">Apakah kamu yakin Mau Menghapus <strong><?php echo $id; ?>?</strong></p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="delete" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YA</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> TIDAK</button>
                            </div>
                        </div>
                </form>
                </div>
            </div>s
                  <?php  } }?>
                      </tbody>
                    </table>
                                <!--add Item Modal -->
                    <div id="addsiswa" class="modal fade" role="dialog">
                        <form method="post" action="t_siswa.php">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                         <h4 class="modal-title">Tambah Pengguna Siswa</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       
                                    </div>
                                    <div class="modal-body">
                                      <div class="form-group">
                                              </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="id">Username</label>
                                                <input type="number" class="form-control" id="username" name="username" aceholder=" ID ortu" >
                                              </div>
                                              <div class="form-group">
                                            <label class="control-label col-sm-3" for="password">Password</label>
                                           <input type="password" class="form-control" id="password" name="pas" laceholder="Nama ortu" >
                                              </div>
                                              <div class="form-group">
                                            <label class="control-label col-sm-3" for="nis">NIS Siswa</label>
                                           <input type="text" class="form-control" id="nis" name="nis" laceholder="NIS Siswa" >
                                              </div>
                                             
                                    <div class="modal-footer">
                                        <button type="submit"  name="tambah" class="btn btn-primary"><span class="glyphicon glyphicon-tambah"></span> tambah</button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </form>
                    </div>


                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>



          </div>
          <div class="clearfix"></div>
        </div>
<?php include('footer.php'); ?>


<?php 

  if(isset($_POST['delete'])){
                           
                            $delete_id = $_POST['delete_id'];
                            $sql = "DELETE FROM user WHERE username ='$delete_id' ";
                            if ($conn->query($sql) === TRUE) {
                                echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Berhasil Dihapus',
                                              type: 'success',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('user.php');
                                    } ,3000); 
                                    </script>";
                            } else {
                               echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Gagal Dihapus',
                                              type: 'error',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('user.php');
                                    } ,3000); 
                                    </script>";
                            
                            }
                        }
 ?>
