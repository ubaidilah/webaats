<?php   
require_once 'akses.php'; 
require_once 'koneksi.php'; 
require_once 'sweetalert.php'; 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Webbaast </title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="vendors/starrr/dist/starrr.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

     <!-- Datatables -->
    <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="beranda.php" class="site_title"><i class="fa fa-paw"></i> <span>Webaast</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="img/profile/default.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?=$_SESSION['admin'];?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">

                  <li><a href="beranda.php"><i class="fa fa-home"></i> Beranda </a>
                  </li>
                  <?php if ($_SESSION['level'] == 'admin') {
                    # code...
                   ?>
                  <li><a href="user.php"><i class="fa fa-user"></i> User </a>
                  </li>
                  <li><a href="siswa.php"><i class="fa fa-user"></i> Siswa </a>
                  </li>
                  <li><a href="guru.php"><i class="fa fa-user"></i> Guru </a>
                  </li>
                  <li><a href="ortu.php"><i class="fa fa-user"></i> Orang Tua Murid </a>
                  </li>
                  <li><a><i class="fa fa-database"></i> Data   <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="kelas.php">Kelas</a></li>
                      <li><a href="mapel.php">Mata Pelajaran</a></li>
                      <li><a href="jurusan.php">Jurusan</a></li>
                    </ul>
                  </li>
                <?php } ?>
                 <li><a href="jadwal.php"><i class="fa fa-calendar-o"></i> Jadwal </a>
                  </li>
                  <li><a href="#"><i class="fa fa-calendar-o"></i>Kehadiran </a>
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="img/profile/default.jpg" alt=""><?=$_SESSION['admin'];?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="profile.php"> Profile</a></li>
                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
     <div class="right_col" role="main">
      <?php $lokpp = 'img/profile/' ?>


      <?php
      $sqlusr = "SELECT * FROM user WHERE username = '".$_SESSION['admin']."'";
      $resultusr = $conn->query($sqlusr);
      $rowuser=mysqli_fetch_object($resultusr);
      if ($_SESSION['level'] == 'admin'){
      $detail_user = $rowuser->id_admin;} 
      if ($_SESSION['level'] == 'guru') {
        $detail_user = $rowuser->nip;
      }
      if ($_SESSION['level'] == 'siswa') {
        $detail_user = $rowuser->nis;
      }
      if ($_SESSION['level'] == 'ortu') {
        $detail_user = $rowuser->id_ortu;
      }

       $sqldtkls = "SELECT * FROM siswa WHERE nis = '$detail_user'";
      $resultsisk = $conn->query($sqldtkls);
      $rowdatkel=mysqli_fetch_object($resultsisk);
      $siswa_kelas = $rowdatkel->kd_kelas;
      
      ?>


