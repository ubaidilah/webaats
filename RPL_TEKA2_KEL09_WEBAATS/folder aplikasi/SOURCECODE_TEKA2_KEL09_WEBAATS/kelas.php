<?php 
require_once 'header.php';
?>

     <!-- page content -->
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2>
                 <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="index.php">Dashboard</a>
                  </li>
            <li class="breadcrumb-item active">Kelas</li>
          </ol></h2>
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Kelas</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                      </li>
                    </ul>
                    <div class="clearfix">
                      

                    </div>

                  </div>
                  <div class="x_content">
                    <center><a href="#add" data-toggle="modal"><button type='button' class='btn btn-success btn-sm'><span class='glyphicon glyphicon-plus' aria-hidden='true'> Tambah Kelas </span></button></a></center>
                    
                        <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                    <tr>
                   <th><center>  No </center></th>
                    <th><center>  Id Kelas </center></th>
                    <th> <center>Nama Kelas </center></th>
                    <th> <center>Nama Guru </center></th>
                    <th> <center>Jurusan </center></th>
                    <th><center>  Aksi </center></th>
                    </tr>
                  </thead>
                       
                      <tbody>

                     <?php 
                     // SELECT `nip`, `nama_guru`, `mapel`, `alamat`, `jk`, `email`, `foto` FROM `guru` WHERE 1
                 
                    $x =1;
                    $sql = "SELECT id_kelas, nama_kelas,guru.nama_guru, jurusan.nama_jurusan,guru.nip FROM kelas
                          LEFT JOIN jurusan ON (kelas.id_jurusan = jurusan.id_jurusan)
                          LEFT JOIN guru ON (kelas.nip_walikelas = guru.nip)";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $id = $row['id_kelas'];
                            $nama = $row['nama_kelas'];
                            $nip = $row['nip'];
                            $nama_gr = $row['nama_guru'];
                            // $kdjurusan = $row['id_jurusan'];
                            $namajurusan = $row['nama_jurusan'];

                       echo "
                    <tr>
      
                      <td><center>$x</center></td>
                      <th><center>$id</center></th>
                      <td><center>$nama</center></td>
                      <td><center>$nama_gr</center></td>
                    
                      <td><center>$namajurusan</center></td>
                       "; 
                    $x++; ?>
                     <td> <center>
                            <a href="#edit<?php echo $id;?>" data-toggle="modal"><button type='button' class='btn btn-warning btn-sm gfa-edit '><span class='glyphicon glyphicon-edit' aria-hidden='true'>Ubah</span></button></a>
                            <a href="#delete<?php echo $id;?>" data-toggle="modal"><button type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-trash' aria-hidden='true'> Hapus</span></button></a>
                     </center>
                    </td>
                  </tr>

                 <!--add Item Modal -->
        <div id="add" class="modal fade" role="dialog">
            <form method="post" action="t_kelas.php">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Tambah Data Kelas</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                                  </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">Id Kelas</label>
                                    <input type="text" class="form-control" id="id" name="id" aceholder=" NIS guru" >
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">nama Kelas</label>
                                    <input type="text" class="form-control" id="nama" name="nama" aceholder=" Nama Kelas" >
                            </div>
                                  
                                  <div class="form-group">
                                    <label class="control-label col-sm-3" for="walikelas">Walikelas </label>
                                <select name="walikelas"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
                                <option Value = '' selected  required>Pilih Nama Guru </option>
                                          <?php 
                                
                                          $sql3 = "SELECT * FROM guru ";
                                          $result3 = $conn->query($sql3);
                                          while($data = $result3->fetch_assoc()) {
                                            $idgr = $data["nip"];
                                            $namagr = $data["nama_guru"];
                                            echo '<option value ="'.$idgr.'">'.$namagr.'</option>';
                                            
                                          }

                                          ?>
                              </select>
                              </div>

                                 
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="jurusan">Jurusan</label>
                                <select name="jurusan"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
                              <option Value = '' selected  required>Pilih Jurusan </option>
                                          <?php 
                                
                                          $sql3 = "SELECT * FROM jurusan ";
                                          $result3 = $conn->query($sql3);
                                          while($data = $result3->fetch_assoc()) {
                                            $idjr = $data["id_jurusan"];
                                            $nmjr = $data["nama_jurusan"];
                                            echo '<option value ="'.$idjr.'">'.$nmjr.'</option>';
                                            
                                          }

                                          ?>
                            </select>
                              </div>
                               
                                
                        <div class="modal-footer">
                            <button type="submit"  name="tambah" class="btn btn-primary"><span class="glyphicon glyphicon-tambah"></span> tambah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>

  <!--Edit Item Modal -->
<div id="edit<?php echo $id; ?>" class="modal fade" role="dialog">
            <form method="post" action="u_kelas.php" >
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Tambah Data Kelas</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                                  </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">Id Kelas</label>
                                    <input type="text" class="form-control" id="id" name="id" value="<?=$id?>" aceholder=" NIS guru" >
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">nama Kelas</label>
                                    <input type="text" class="form-control" id="nama" name="nama" value="<?=$nama?>" aceholder=" Nama Kelas" >
                            </div>
                                  
                                  <div class="form-group">
                                    <label class="control-label col-sm-3" for="walikelas">Walikelas </label>
                                <select name="walikelas"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
                                <option Value = '<?=$nip?>' selected  required><?=$nama_gr?></option>
                                          <?php 
                                
                                          $sql3 = "SELECT * FROM guru ";
                                          $result3 = $conn->query($sql3);
                                          while($data = $result3->fetch_assoc()) {
                                            $idgr = $data["nip"];
                                            $namagr = $data["nama_guru"];
                                            echo '<option value ="'.$idgr.'">'.$namagr.'</option>';
                                            
                                          }

                                          ?>
                              </select>
                              </div>

                                 
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="jurusan">Jurusan</label>
                                <select name="jurusan"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
                              <option Value = '' selected  required>Pilih Jurusan </option>
                                          <?php 
                                
                                          $sql3 = "SELECT * FROM jurusan ";
                                          $result3 = $conn->query($sql3);
                                          while($data = $result3->fetch_assoc()) {
                                            $idjr = $data["id_jurusan"];
                                            $nmjr = $data["nama_jurusan"];
                                            echo '<option value ="'.$idjr.'">'.$nmjr.'</option>';
                                            
                                          }

                                          ?>
                            </select>
                              </div>
                               
                                
                        <div class="modal-footer">
                            <button type="submit"  name="ubah" class="btn btn-primary"><span class="glyphicon glyphicon-tambah"></span> Ubah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
                  <!-- delete Unit  Modal-->
     <div id="delete<?php echo $id; ?>" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form method="post">
                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">
                           <h4 class="modal-title">Delete</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                        </div>

                        <div class="modal-body">
                            <input type="hidden" name="delete_id" value="<?php echo $id; ?>">
                            <p>
                                <div class="alert alert-danger">Apakah kamu yakin Mau Menghapus <strong><?php echo $nama; ?>?</strong></p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="delete" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YA</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> TIDAK</button>
                            </div>
                        </div>
                </form>
                </div>
            </div>
         
    
                    <?php  } }?>


                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->


<?php 

  if(isset($_POST['delete'])){
                           
                            $delete_id = $_POST['delete_id'];
                            $sql = "DELETE FROM kelas WHERE id_kelas ='$delete_id' ";
                            if ($conn->query($sql) === TRUE) {
                                echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Berhasil Dihapus',
                                              type: 'success',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('kelas.php');
                                    } ,3000); 
                                    </script>";
                            } else {
                               echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Gagal Dihapus',
                                              type: 'error',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('kelas.php');
                                    } ,3000); 
                                    </script>";
                            
                            }
                        }
 ?>







<?php
require_once 'footer.php';
?>