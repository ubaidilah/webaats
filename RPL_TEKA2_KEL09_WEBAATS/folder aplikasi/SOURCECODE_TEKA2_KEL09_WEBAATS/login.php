<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.all.min.js"></script>
  <link rel="icon" type="image/png" href="img/logo.ico"/>

  <title>Login</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body style="background-repeat: no-repeat; background-size: cover;background-position: center;background-image: url(img/bg.jpg); ">

<div class="container">

<!-- Outer Row -->
<div class="row justify-content-center">

  <div class="col-lg-7">

    <div class="card 1-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lgd-none d-lg-block bg-login-image"></div>
          <div class="col-lg">
            <div class="p-5">
              <div class="text-center">
                <img src="img/logo.png" alt="Logo " width="100" height="100">
              </div>
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4" style="font-family:Poppins-Bold; text-transform: uppercase;font-size:30px;">ATTENDANCE SYSTEM <br />Sign In</h1>
              </div>
              <form method="post" action="p_login.php" class="needs-validation" novalidate >
                <div class="form-group">
                  <input type="text" name="username" class="form-control form-control-user" id="username" aria-describedby="username" placeholder="Masukan Username" required="">
                     <div class="invalid-feedback">Harus diisi</div>
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control form-control-user" id="password" placeholder="Password" required>
                     <div class="invalid-feedback">Harus diisi !</div>
                </div>
                <button type="submit" name="login" value="login" class="btn btn-primary btn-user btn-block ">
                  Login
                </button>
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="index.php">Kembali</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</div>

</div>


<script>
(function() {
  'use strict';
  window.addEventListener('load', function() {
    var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>



