<?php 
require_once 'header.php';

?>
     <!-- page content -->
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2>
                 <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="beranda.php">Dashboard</a>
                  </li>
            <li class="breadcrumb-item active">Jurusan</li>
          </ol></h2>
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data jurusan</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                      </li>
                    </ul>
                    <div class="clearfix">
                      

                    </div>

                  </div>
                  <div class="x_content">
                    <center><a href="#add" data-toggle="modal"><button type='button' class='btn btn-success btn-sm'><span class='glyphicon glyphicon-plus' aria-hidden='true'> Tambah Jurusan </span></button></a></center>
                    
                      <table id="datatable" class="table table-striped table-bordered" style="text-align: center;">
                      <thead>
                    <tr>
                   <th>  No </th>
                    <th>  Id Jurusan </th>
                    <th> Nama Jurusan </th>
                    <th>  aksi </th>
                    </tr>
                  </thead>
                       
                      <tbody>

                     <?php 
                  
                    $x =1;
                    $sql = "SELECT * FROM jurusan ";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $id = $row['id_jurusan'];
                            $jurusan = $row['nama_jurusan'];
                       echo "
                    <tr>
      
                      <td><center>$x</center></td>
                      <td><center>$id</center></td>
                      <td><center>$jurusan</center></td>

                       "; 
                    $x++; ?>
                     <td> <center>
                            <a href="#edit<?php echo $id;?>" data-toggle="modal"><button type='button' class='btn btn-warning btn-sm gfa-edit '><span class='glyphicon glyphicon-edit' aria-hidden='true'>Ubah</span></button></a>
                            <a href="#delete<?php echo $id;?>" data-toggle="modal"><button type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-trash' aria-hidden='true'> Hapus</span></button></a>
                     </center>
                    </td>
                  </tr>


   <!--add Item Modal -->
        <div id="add" class="modal fade" role="dialog">
            <form method="post" action="t_jurusan.php">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Tambah Data Jurusan</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                                  </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">Id Jurusan</label>
                                    <input type="number" class="form-control" id="tid" name="tid" aceholder=" ID ortu" >
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="tambahortu">Nama Jurusan</label>
                               <input type="text" class="form-control" id="tambahortu" name="namajurusan" laceholder="Nama ortu" >
                                  </div>
                                 
                        <div class="modal-footer">
                            <button type="submit"  name="tambah" class="btn btn-primary"><span class="glyphicon glyphicon-tambah"></span> tambah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>

  <!--Edit Item Modal -->
        <div id="edit<?php echo $id; ?>" class="modal fade" role="dialog">
            <form method="post" action="u_ortu.php" >
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Pembaharuan Data Jurusan</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <!--  // SELECT `id_ortu`, `nama`, `kd_nis`, `alamat`, `no_hp`, `foto`, `jk`, `email` FROM `orang_tua` WHERE 1 -->
                        </div>
                        <div class="modal-body">
          
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">Id</label>
                                    <input type="number" class="form-control" id="editortu" name="editortu" value="<?php echo $id; ?>" placeholder="NIS" readonly>
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="editnama">Nama Jurusan </label>
                               <input type="text" class="form-control" id="editnama" name="editnama" value="<?php echo $jurusan; ?>" placeholder="Nama ortu" >
                                  </div>
                                  
                        <div class="modal-footer">
                            <button type="submit"  name="ubah" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span> Ubah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
                  <!-- delete Unit  Modal-->
     <div id="delete<?php echo $id; ?>" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form method="post">
                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">
                           <h4 class="modal-title">Delete</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                        </div>

                        <div class="modal-body">
                            <input type="hidden" name="delete_id" value="<?php echo $id; ?>">
                            <p>
                                <div class="alert alert-danger">Apakah kamu yakin Mau Menghapus <strong><?php echo $jurusan; ?>?</strong></p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="delete" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YA</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> TIDAK</button>
                            </div>
                        </div>
                </form>
                </div>
            </div>
         
    
                    <?php  } }?>

                 

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->


<?php 

  if(isset($_POST['delete'])){
                           
                            $delete_id = $_POST['delete_id'];
                            $sql = "DELETE FROM jurusan WHERE id_jurusan ='$delete_id' ";
                            if ($conn->query($sql) === TRUE) {
                                echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Berhasil Dihapus',
                                              type: 'success',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('jurusan.php');
                                    } ,3000); 
                                    </script>";
                            } else {
                               echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Gagal Dihapus',
                                              type: 'error',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('jurusan.php');
                                    } ,3000); 
                                    </script>";
                            
                            }
                        }
 ?>

<?php
require_once 'footer.php';
?>
<?php
if(isset($_POST['tambah'])){
                            $idt = $_POST['id'];
                            $jurusant = $_POST['jurusan'];
  
                $queryCheck = $conn->query("SELECT * FROM jurusan WHERE id_jurusan = '$idt' " );
                      if($queryCheck->num_rows == 0){ 

                     $sql ="INSERT INTO jurusan SET 
                                  id_jurusan='$idt',
                                  nama_jurusan='$jurusant'";

                            if ($conn->query($sql) === TRUE) {
                               echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Berhasil Dimasukkan',
                                              type: 'success',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('jurusan.php');
                                    } ,3000); 
                                    </script>";
                            } else {
                               echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Gagal Dimasukkan!',
                                              type: 'error',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('jurusan.php');
                                    } ,3000); 
                                    </script>";
        }
      }
    }
?>