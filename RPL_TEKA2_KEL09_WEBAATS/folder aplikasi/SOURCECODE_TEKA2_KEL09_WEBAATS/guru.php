<?php 
require_once 'header.php';

?>

     <!-- page content -->
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2>
                 <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="index.php">Dashboard</a>
                  </li>
            <li class="breadcrumb-item active">Guru</li>
          </ol></h2>
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Guru</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                      </li>
                    </ul>
                    <div class="clearfix">
                      

                    </div>

                  </div>
                  <div class="x_content">
                    <center><a href="#add" data-toggle="modal"><button type='button' class='btn btn-success btn-sm'><span class='glyphicon glyphicon-plus' aria-hidden='true'> Tambah Guru </span></button></a></center>
                    
                        <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                    <tr>
                   <th><center>  No </center></th>
                    <th><center>  NIP </center></th>
                    <th> <center>Nama </center></th>
                    <th> <center>Guru Mapel </center></th>
                    <th> <center>JK </center></th>
                    <th> <center>foto</center></th>
                    <th><center>  Action </center></th>
                    </tr>
                  </thead>
                       
                      <tbody>

                     <?php 
                     // SELECT `nip`, `nama_guru`, `mapel`, `alamat`, `jk`, `email`, `foto` FROM `guru` WHERE 1
                 
                    $x =1;
                    $sql = "SELECT nip, nama_guru,mapel, alamat , jk, email,foto,  mapel.nama_mapel,mapel.id_mapel FROM guru INNER JOIN mapel ON (guru.mapel = mapel.id_mapel ) ";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $nip = $row['nip'];
                            $nama = $row['nama_guru'];
                            $alamat = $row['alamat'];
                            $idmapel = $row['mapel'];
                            $mapel = $row['nama_mapel'];
                            $kdmapel = $row['id_mapel'];
                            $email = $row['email'];
                            $jk = $row['jk'];
                            $foto = $row['foto'];

                       echo "
                    <tr>
      
                      <td><center>$x</center></td>
                      <th><center>$nip</center></th>
                      <td><center>$nama</center></td>
                      <td><center>$mapel</center></td>
                    
                      <td><center>$jk</center></td>
                      <td><center>$foto</center></td>
                       "; 
                    $x++; ?>
                     <td> <center>
                            <a href="#edit<?php echo $nip;?>" data-toggle="modal"><button type='button' class='btn btn-warning btn-sm gfa-edit '><span class='glyphicon glyphicon-edit' aria-hidden='true'>Ubah</span></button></a>
                            <a href="#delete<?php echo $nip;?>" data-toggle="modal"><button type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-trash' aria-hidden='true'> Hapus</span></button></a>
                     </center>
                    </td>
                  </tr>

                 <!--add Item Modal -->
        <div id="add" class="modal fade" role="dialog">
            <form method="post" action="t_guru.php">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Tambah Data guru</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                                  </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">NIP</label>
                                    <input type="number" class="form-control" id="tnip" name="tnip" aceholder=" NIS guru" >
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="tambahguru">Nama guru</label>
                               <input type="text" class="form-control" id="tambahguru" name="tambahnama" laceholder="Nama guru" >
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-3" for="tkelas">mapel</label>
			                          <select name="tmapel"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
			                   			<option Value = '' selected  required>Pilih Mapel </option>
			                                    <?php 
			                          
			                                    $sql3 = "SELECT * FROM mapel ";
			                                    $result3 = $conn->query($sql3);
			                                    while($data = $result3->fetch_assoc()) {
			                                      $idmpl = $data["id_mapel"];
			                                      $mpl = $data["nama_mapel"];
			                                      echo '<option value ="'.$idmpl.'">'.$mpl.'</option>';
			                                      
			                                    }

			                                    ?>
			               				</select>
			                        </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="tambahtgllahir">Tanggal Lahir</label>
                              	<input type="date" class="form-control" id="tambahtgllahir" name="tambahtgl_lahir"  >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="tambahalmt">Alamat</label>
                              	<input type="text" class="form-control" id="tambahalmt" name="tambahalamat"  >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="tambaheml">Email</label>
                              	<input type="text" class="form-control" id="tambaheml" name="tambahemail" >
                                </div>
                                <div class="form-group">
                                	 <label class="control-label col-sm-3" for="tambahhp">Jenis Kelamin</label>
                               		 <select name="tambahjk"   class="form-control selectpicker" data-live-search="true"  required >
			                   			<option Value = 'L' selected  required>Laki - Laki </option>
			                   			<option Value = 'P'   required>Perempuan</option>
			                                
			               			</select>
			               		</div>
                                
                        <div class="modal-footer">
                            <button type="submit"  name="tambah" class="btn btn-primary"><span class="glyphicon glyphicon-tambah"></span> tambah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>

  <!--Edit Item Modal -->
        <div id="edit<?php echo $nip; ?>" class="modal fade" role="dialog">
            <form method="post" action="u_guru.php" >
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Pembaharuan Data guru</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         
                        </div>
                        <div class="modal-body">
          
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">NIP</label>
                                    <input type="number" class="form-control" id="editnip" name="editnip" value="<?php echo $nip; ?>" placeholder="NIS" readonly>
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="editnama">Nama guru</label>
                               <input type="text" class="form-control" id="editnama" name="editnama" value="<?php echo $nama; ?>" placeholder="Nama guru" >
                                  </div>
                                   <div class="form-group">
                                    <label class="control-label col-sm-3" for="tkelas">Mapel</label>
			                          <select name="editmapel"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
			                   			<option Value = '<?= $kdmapel; ?>' selected  required><?= $mapel; ?> </option>
			                                    <?php 
			              
			                          
			                                    $sql4 = "SELECT * FROM mapel ";
			                                    $result3 = $conn->query($sql4);
			                                    while($data = $result3->fetch_assoc()) {
			                                      $idmpl = $data["id_mapel"];
			                                      $mpl = $data["nama_mapel"];
			                                      echo '<option value ="'.$idmpl.'">'.$mpl.'</option>';
			                                      
			                                    }

			                                    ?>
			               				</select>
			                        </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editalamat">Alamat</label>
                              	<input type="text" class="form-control" id="editalamat" name="editalamat" value="<?php echo $alamat; ?>" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editemail">Email</label>
                              	<input type="text" class="form-control" id="editemail" name="editemail" value="<?php echo $email; ?>" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editjk">Jenis Kelamin</label>
                                <select name="editjk"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
			                   			<option Value = '<?= $jk ?>'   required><?php if ($jk == 'L') 
			                   				echo "Laki - Laki";
			                   			else echo "Perempuan" ?></option>
			                   			<option Value = 'L'   required>Laki - Laki </option>
			                   			<option Value = 'P'   required>Perempuan</option>
			                                
			               			</select>
                                </div>
                                <div class="form-group">
                                	<img src="">
                              	<input type="hidden" class="form-control" id="edithead" name="editfoto" value="<?php echo $foto; ?>" >
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit"  name="ubah" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span> Ubah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
                  <!-- delete Unit  Modal-->
     <div id="delete<?php echo $nip; ?>" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form method="post">
                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">
                           <h4 class="modal-title">Delete</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                        </div>

                        <div class="modal-body">
                            <input type="hidden" name="delete_id" value="<?php echo $nip; ?>">
                            <p>
                                <div class="alert alert-danger">Apakah kamu yakin Mau Menghapus <strong><?php echo $nama; ?>?</strong></p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="delete" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YA</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> TIDAK</button>
                            </div>
                        </div>
                </form>
                </div>
            </div>
         
    
                    <?php  } }?>


                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->


<?php 

  if(isset($_POST['delete'])){
                           
                            $delete_id = $_POST['delete_id'];
                            $sql = "DELETE FROM guru WHERE nip ='$delete_id' ";
                            if ($conn->query($sql) === TRUE) {
                                echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Berhasil Dihapus',
                                              type: 'success',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('guru.php');
                                    } ,3000); 
                                    </script>";
                            } else {
                               echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Gagal Dihapus',
                                              type: 'error',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('guru.php');
                                    } ,3000); 
                                    </script>";
                            
                            }
                        }
 ?>







<?php
require_once 'footer.php';
?>