<?php 
require_once 'header.php';
require_once 'hari.php'; 
?>


        <!-- page content -->
          <div class="">
    <div class="row top_tiles">
      <?php if ( $_SESSION['level'] == 'guru') {
        ?>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-users"></i></div>

            <div class="count">0</div>
            <h3>Siswa  </h3>
            <p>Jumlah Siswa </p>
          </div>
        </div>
      <?php }?>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-users"></i></div>
            <div class="count">0</div>
            <h3> Izin </h3>
            <p>Jumlah Izin dalam satu semester</p>
          </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="glyphicon glyphicon-time"></i></div>

            <div class="count">0</div>
            <h3> Tidak Hadir </h3>
            <p> Jumlah Alpa dalam satu semester</p>
          </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
          <div class="icon"><i class="glyphicon glyphicon-time"></i></div>
            <div class='count'>0</div>
            <h3> Sakit  </h3>
            <p>  Jumlah Sakit dalam satu semester</p>
          </div>
        </div>
      </div>


  <?php if ( $_SESSION['level'] == 'guru') {
        ?>

    	 <div class="row">
              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Jadwal Pelajaran <small>Hari ini </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <?php $datajdw = "SELECT id_jadwal, id_kelas, waktu_masuk, waktu_keluar, guru.nip, hari,mapel.nama_mapel,mapel.id_mapel, guru.nama_guru FROM jadwal 
                  INNER JOIN mapel ON (jadwal.id_mapel = mapel.id_mapel )
                  INNER JOIN guru ON (jadwal.nip = guru.nip )
                  INNER JOIN siswa ON (jadwal.id_kelas = siswa.kd_kelas )
                   WHERE  hari = '$hari' ";

                    $resultjdwl =$conn->query($datajdw);
                      if ($resultjdwl->num_rows > 0) {
                        while ($rowj = $resultjdwl->fetch_assoc()) {
                          $id = $rowj['id_jadwal'];
                          $kdmapel = $rowj['id_mapel'];
                          $mapel = $rowj['nama_mapel'];
                          $guru = $rowj['nama_guru'];
                          $nip = $rowj['nip'];
                          $kdkelas = $rowj['id_kelas'];
                          $wmasuk = $rowj['waktu_masuk'];
                          $wkeluar = $rowj['waktu_keluar'];
                          $nipj = $rowj['nip'];
                          $hr = $rowj['hari']; ?>

                  <div class="x_content">
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month"><?= date ("F"); ?></p>
                        <p class="day"><?= date ("d");?></p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#"><?=$mapel;?></a>
                        <p><?=$guru;?></p>
                        <form method="post" action="scanner.php">
                          <input type="hidden" name="idj" value="<?=$id?>">
                          <input type="hidden" name="nipj" value="<?=$nip?>">
                          <input type="submit" name="scan" value="scan QR">
                        </form>
                      </div>
                    </article>
                  <?php } } else { echo "Tidak Ada Jadwal";}?>

                  </div>
                </div>
              </div>
        </div>
  <?php   } ?>
   <?php if ( $_SESSION['level'] == 'siswa') {
        ?>

  <div class="row">
              <div class="col-md-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Jadwal Pelajaran <small>Hari ini </small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <?php $datajdw = "SELECT id_jadwal, id_kelas, waktu_masuk, waktu_keluar, guru.nip, hari,mapel.nama_mapel,mapel.id_mapel, guru.nama_guru FROM jadwal 
                  INNER JOIN mapel ON (jadwal.id_mapel = mapel.id_mapel )
                  INNER JOIN guru ON (jadwal.nip = guru.nip )
                  INNER JOIN siswa ON (jadwal.id_kelas = siswa.kd_kelas )
                   WHERE  hari = '$hari' and id_kelas = '$siswa_kelas' ";

                    $resultjdwl =$conn->query($datajdw);
                      if ($resultjdwl->num_rows > 0) {
                        while ($rowj = $resultjdwl->fetch_assoc()) {
                          $id = $rowj['id_jadwal'];
                          $kdmapel = $rowj['id_mapel'];
                          $mapel = $rowj['nama_mapel'];
                          $guru = $rowj['nama_guru'];
                          $nip = $rowj['nip'];
                          $kdkelas = $rowj['id_kelas'];
                          $wmasuk = $rowj['waktu_masuk'];
                          $wkeluar = $rowj['waktu_keluar'];
                          $nipj = $rowj['nip'];
                          $hr = $rowj['hari']; ?>

                  <div class="x_content">
                    <article class="media event">
                      <a class="pull-left date">
                        <p class="month"><?= date ("F"); ?></p>
                        <p class="day"><?= date ("d");?></p>
                      </a>
                      <div class="media-body">
                        <a class="title" href="#"><?=$mapel;?></a>
                        <p><?=$guru;?></p>
                       
                      </div>
                    </article>
                  <?php } } else { echo "Tidak Ada Jadwal";}?>

                  </div>
                </div>
              </div>
       
          </div>
           <?php }?>
        <!-- /page content -->
      </div>
    </div>
       
<?php
require_once 'footer.php';
?>
