 <?php 
require_once 'header.php';
require_once 'hari.php'; 



?>
  
          
        <?php 
        // SELECT `nis`, `nama_siswa`, `kd_kelas`, `kd_ortu`, `tgl_lahir`, `alamat`, `no_hp`, `email`, `jk`, `foto`, `akses` FROM `siswa` WHERE 1
          $sqlprof = "SELECT nis, nama_siswa, tgl_lahir, siswa.alamat, siswa.no_hp, siswa.email, siswa.jk, siswa.foto, kelas.nama_kelas, orang_tua.nama FROM siswa LEFT JOIN kelas ON (siswa.kd_kelas = kelas.id_kelas) LEFT JOIN orang_tua ON (siswa.kd_ortu = orang_tua.id_ortu) WHERE nis = '$detail_user' ";
      $resultprof = $conn->query($sqlprof);
      $rowprof=mysqli_fetch_object($resultprof);
      $namapr = $rowprof->nama_siswa;
      $alamatpr = $rowprof->alamat;
      $namakls = $rowprof->nama_kelas;
      $namaortu = $rowprof->nama;

        ?>

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Profil</h3>
              </div>

            
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Profil  <small>Data Pribadi</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-12 col-sm-12 col-xs-12 profile_center">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="img/profile/default.jpg" alt="Avatar" title="Change the avatar" sizes="50%">
                        </div>
                      </div>
                      <h3><?=$namapr?></h3>

                      <ul class="list-unstyled user_data">

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i>TEK 54
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-external-link user-profile-icon"></i>
                          <a href="http://www.kimlabs.com/profile/" target="_blank">Webaast</a>
                        </li>
                        <li><i class="fa fa-map-marker user-profile-icon"></i> <?=$alamatpr?>
                        </li>
                      </ul>

                      <a class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
                      <br />

                      <!-- start skills -->
                      <h4>Skills</h4>
                      <ul class="list-unstyled user_data">
                        <li>
                          <p>Web Applications</p>
                          <div class="progress progress_sm">
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                          </div>
                        </li>
                        <li>
                          <p>Website Design</p>
                          <div class="progress progress_sm">
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="70"></div>
                          </div>
                        </li>
                        <li>
                          <p>Automation & Testing</p>
                          <div class="progress progress_sm">
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="30"></div>
                          </div>
                        </li>
                        <li>
                          <p>UI / UX</p>
                          <div class="progress progress_sm">
                            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                          </div>
                        </li>
                      </ul>
                      <!-- end of skills -->

                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
   

              <?php 
require_once 'footer.php';

?> 