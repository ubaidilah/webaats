<?php 
require_once 'header.php';
require_once 'hari.php';


?>
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css"> -->

     <!-- page content -->
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2>
                 <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="index.php">Dashboard</a>
                  </li>
            <li class="breadcrumb-item active">Jadwal</li>
          </ol></h2>
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Jadwal</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                      </li>
                    </ul>
                    <div class="clearfix">
                      

                    </div>

                  </div>
                  <div class="x_content">
                     <?php if ( $_SESSION['level'] == 'siswa') {?>
                    <p class="text-muted font-13 m-b-30">
                      Jadwal Mata Pelajaran
                    </p>
                    <center><a href="tabel.php" ><button type='button' class='btn btn-success btn-sm'><span class='glyphicon glyphicon-plus' > PDF </span></button></a></center>
                    <table id="jadwal" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>NO</th>
                          <th>Jam</th>
                          <th>Senin</th>
                          <th>Selasa</th>
                          <th>Rabu</th>
                          <th>Kamis</th>
                          <th>Jumat</th>
                          <th>Sabtu</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $y=1;
                        $datajdwsis = "SELECT id_jadwal, id_kelas, waktu_masuk, waktu_keluar, guru.nip, hari,mapel.nama_mapel,mapel.id_mapel, guru.nama_guru FROM jadwal 
                  LEFT JOIN mapel ON (jadwal.id_mapel = mapel.id_mapel )
                  LEFT JOIN guru ON (jadwal.nip = guru.nip ) WHERE id_kelas = '$siswa_kelas' ORDER BY jadwal.waktu_masuk ASC
                    "; 
                   

                   $resultjdwlsis =$conn->query($datajdwsis);
                      if ($resultjdwlsis->num_rows > 0) {
                        while ($rowj = $resultjdwlsis->fetch_assoc()) {
                          $id = $rowj['id_jadwal'];
                          $kdmapel = $rowj['id_mapel'];
                          $mapel = $rowj['nama_mapel'];
                          $guru = $rowj['nama_guru'];
                          $nip = $rowj['nip'];
                          $kdkelas = $rowj['id_kelas'];
                          $wmasuk = $rowj['waktu_masuk'];
                          $wkeluar = $rowj['waktu_keluar'];
                          $nipj = $rowj['nip'];
                          $hr = $rowj['hari']; 

                       echo "
                    <tr>
      
                      <td><center>$y</center></td>
                      <td><center>$wmasuk - $wkeluar</center></td>
                      
                      
                      

                       "; 
                       if ('Senin' == $hr){
                           echo "<td><center>$mapel</center></td>";
                       }else{
                        echo "<td><center></center></td>";
                       }
                      if ('Selasa' == $hr){
                           echo "<td><center>$mapel</center></td>";
                       }else{
                        echo "<td><center></center></td>";
                       }
                       

                       if ('Rabu' == $hr){
                           echo "<td><center>$mapel</center></td>";
                       }else{
                        echo "<td><center></center></td>";
                       }
                       if ('Kamis' == $hr){
                           echo "<td><center>$mapel</center></td>";
                       }else{
                        echo "<td><center></center></td>";
                       }
                       if ('Jumat' == $hr){
                           echo "<td><center>$mapel</center></td>";
                       }else{
                        echo "<td><center></center></td>";
                       }
                       if ('Sabtu' == $hr){
                           echo "<td><center>$mapel</center></td>";
                       }else{
                        echo "<td><center></center></td>";
                       }

                    $y++; }}?>
                      </tbody>
                    </table>

             <?php }
             if ( $_SESSION['level'] == 'admin') {?>
                    <center><a href="#add" data-toggle="modal"><button type='button' class='btn btn-success btn-sm'><span class='glyphicon glyphicon-plus' aria-hidden='true'> Tambah Jadwal </span></button></a></center> 
                    
                        <table id="myTable" class="table table-striped table-bordered">
                      <thead>
                    <tr>
                   <th><center>  No </center></th>
                    <th><center>  ID Jadwal </center></th>
                    <th><center>  Hari </center></th>
                    <th> <center>Mata Pelajaran </center></th>
                    <th> <center>Waktu Masuk </center></th>
                    <th> <center>Waktu Keluar  </center></th>
                    <th> <center>Guru</center></th>
                    <?php if ( $_SESSION['level'] == 'admin') {?>
                    <th><center>  Aksi </center></th> <?php }?>
                    </tr>
                  </thead>
                       
                      <tbody>

                    <?php 
                    $x=1;
                    $datajdw = "SELECT id_jadwal, id_kelas, waktu_masuk, waktu_keluar, guru.nip, hari,mapel.nama_mapel,mapel.id_mapel, guru.nama_guru FROM jadwal 
                  LEFT JOIN mapel ON (jadwal.id_mapel = mapel.id_mapel )
                  LEFT JOIN guru ON (jadwal.nip = guru.nip )
                    ";


                    $resultjdwl =$conn->query($datajdw);
                      if ($resultjdwl->num_rows > 0) {
                        while ($rowj = $resultjdwl->fetch_assoc()) {
                          $id = $rowj['id_jadwal'];
                          $kdmapel = $rowj['id_mapel'];
                          $mapel = $rowj['nama_mapel'];
                          $guru = $rowj['nama_guru'];
                          $nip = $rowj['nip'];
                          $kdkelas = $rowj['id_kelas'];
                          $wmasuk = $rowj['waktu_masuk'];
                          $wkeluar = $rowj['waktu_keluar'];
                          $nipj = $rowj['nip'];
                          $hr = $rowj['hari']; 

                       echo "
                    <tr>
      
                      <td><center>$x</center></td>
                      <th><center>$id</center></th>
                      <th><center>$hr</center></th>
                      <td><center>$mapel</center></td>
                      <td><center>$wmasuk</center></td>
                      <td><center>$wkeluar</center></td>
                      <td><center>$guru</center></td>
                    
                     
                       "; 
                    $x++; ?>
                     
                     <td> <center>
                            <a href="#edit<?php echo $nip;?>" data-toggle="modal"><button type='button' class='btn btn-warning btn-sm gfa-edit '><span class='glyphicon glyphicon-edit' aria-hidden='true'>Ubah</span></button></a>
                            <a href="#delete<?php echo $nip;?>" data-toggle="modal"><button type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-trash' aria-hidden='true'> Hapus</span></button></a>
                     </center>
                    </td>
                
                  </tr>

              


   

  <!--Edit Item Modal -->
        <div id="edit<?php echo $nip; ?>" class="modal fade" role="dialog">
            <form method="post" action="u_jadwal.php" >
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Pembaharuan Data jadwal</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         
                        </div>
                        <div class="modal-body">
          
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">NIP</label>
                                    <input type="number" class="form-control" id="editnip" name="editnip" value="<?php echo $nip; ?>" placeholder="NIS" readonly>
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="editnama">Nama jadwal</label>
                               <input type="text" class="form-control" id="editnama" name="editnama" value="<?php echo $nama; ?>" placeholder="Nama jadwal" >
                                  </div>
                                   <div class="form-group">
                                    <label class="control-label col-sm-3" for="tkelas">Mapel</label>
			                          <select name="editmapel"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
			                   			<option Value = '<?= $kdmapel; ?>' selected  required><?= $mapel; ?> </option>
			                                    <?php 
			              
			                          
			                                    $sql4 = "SELECT * FROM mapel ";
			                                    $result3 = $conn->query($sql4);
			                                    while($data = $result3->fetch_assoc()) {
			                                      $idmpl = $data["id_mapel"];
			                                      $mpl = $data["nama_mapel"];
			                                      echo '<option value ="'.$idmpl.'">'.$mpl.'</option>';
			                                      
			                                    }

			                                    ?>
			               				</select>
			                        </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editalamat">Alamat</label>
                              	<input type="text" class="form-control" id="editalamat" name="editalamat" value="<?php echo $alamat; ?>" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editemail">Email</label>
                              	<input type="text" class="form-control" id="editemail" name="editemail" value="<?php echo $email; ?>" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editjk">Jenis Kelamin</label>
                                <select name="editjk"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
			                   			<option Value = '<?= $jk ?>'   required><?php if ($jk == 'L') 
			                   				echo "Laki - Laki";
			                   			else echo "Perempuan" ?></option>
			                   			<option Value = 'L'   required>Laki - Laki </option>
			                   			<option Value = 'P'   required>Perempuan</option>
			                                
			               			</select>
                                </div>
                                <div class="form-group">
                                	<img src="">
                              	<input type="hidden" class="form-control" id="edithead" name="editfoto" value="<?php echo $foto; ?>" >
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit"  name="ubah" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span> Ubah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
                  <!-- delete Unit  Modal-->
     <div id="delete<?php echo $nip; ?>" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form method="post">
                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">
                           <h4 class="modal-title">Delete</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                        </div>

                        <div class="modal-body">
                            <input type="hidden" name="delete_id" value="<?php echo $nip; ?>">
                            <p>
                                <div class="alert alert-danger">Apakah kamu yakin Mau Menghapus <strong><?php echo $nama; ?>?</strong></p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="delete" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YA</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> TIDAK</button>
                            </div>
                        </div>
                </form>
                </div>
            </div>
         
    
                    <?php  } }?>


                      </tbody>
                    </table>
                    <?php }?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->

           <!--add Item Modal -->
        <div id="add" class="modal fade" role="dialog">
            <form method="post" action="t_jadwal.php">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Tambah Data jadwal</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                                  </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">KD Jadwal</label>
                                    <input type="text" class="form-control" id="id" name="id" aceholder="  jadwal" >
                                  </div>
                                  
                                  <div class="form-group">
                                    <label class="control-label col-sm-3" for="tkelas">Nama Mapel</label>
                                <select name="tmapel"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
                              <option Value = '' selected  required>Pilih Mapel </option>
                                          <?php 
                                
                                          $sql3 = "SELECT * FROM mapel ";
                                          $result3 = $conn->query($sql3);
                                          while($data = $result3->fetch_assoc()) {
                                            $idmpl = $data["id_mapel"];
                                            $mpl = $data["nama_mapel"];
                                            echo '<option value ="'.$idmpl.'">'.$mpl.'</option>';
                                            
                                          }

                                          ?>
                            </select>
                              </div>
                               <div class="form-group">
                                    <label class="control-label col-sm-3" for="tguru">Guru</label>
                                <select name="tguru"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
                              <option Value = '' selected  required>Pilih Guru </option>
                                          <?php 
                                
                                          $sql3 = "SELECT * FROM guru ";
                                          $result3 = $conn->query($sql3);
                                          while($data = $result3->fetch_assoc()) {
                                            $nip = $data["nip"];
                                            $nama_guru = $data["nama_guru"];
                                            echo '<option value ="'.$nip.'">'.$nama_guru.'</option>';
                                            
                                          }

                                          ?>
                            </select>
                              </div>
                              <div class="form-group">
                                    <label class="control-label col-sm-3" for="tkelas">Kelas</label>
                                <select name="tkelas"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
                              <option Value = '' selected  required>Pilih Kelas </option>
                                          <?php 
                                
                                          $sql3 = "SELECT * FROM kelas ";
                                          $result3 = $conn->query($sql3);
                                          while($data = $result3->fetch_assoc()) {
                                            $idkls = $data["id_kelas"];
                                            $nmakelas = $data["nama_kelas"];
                                            echo '<option value ="'.$idkls.'">'.$nmakelas.'</option>';
                                            
                                          }

                                          ?>
                            </select>
                              </div>
                               <div class="form-group">
                                   <label class="control-label col-sm-3" for="thari">Hari</label>
                                   <select name="thari"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
                              <option value="" selected  required>Pilih Hari</option>
                              <option Value = 'Senin'   required>Senin</option>
                              <option Value = 'Selasa'   required>Selasa</option>
                              <option Value = 'Rabu'   required>Rabu</option>
                              <option Value = 'Kamis'   required>Kamis</option>
                              <option Value = 'Jumat'   required>Jumat</option>
                              <option Value = 'Sabtu'   required>Sabtu</option>
                                      
                                </select>
                              </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="wkmasuk">Jam Masuk</label>
                               <input type="time" class="form-control" id="wkmasuk" name="tmasuk" laceholder="Nama jadwal" >
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="wkmasuk">Jam Keluar</label>
                               <input type="time" class="form-control" id="wkmasuk" name="tkeluar" laceholder="Nama jadwal" >
                                  </div>
                                
                        <div class="modal-footer">
                            <button type="submit"  name="tambah" class="btn btn-primary"><span class="glyphicon glyphicon-tambah"></span> tambah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
        



<?php 

  if(isset($_POST['delete'])){
                           
                            $delete_id = $_POST['delete_id'];
                            $sql = "DELETE FROM jadwal WHERE nip ='$delete_id' ";
                            if ($conn->query($sql) === TRUE) {
                                echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Berhasil Dihapus',
                                              type: 'success',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('jadwal.php');
                                    } ,3000); 
                                    </script>";
                            } else {
                               echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Gagal Dihapus',
                                              type: 'error',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('jadwal.php');
                                    } ,3000); 
                                    </script>";
                            
                            }
                        }
 ?>




<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#jadwal').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'pdfHtml5'
        ]
    } );
} );
</script>
</script>

<?php
require_once 'footer.php';
?>