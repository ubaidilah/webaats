<?php 
if(isset($_SESSION['admin'])){
	
header('Location: http://localhost/final/beranda.php');
}
?>

<html>
	<head>
		<meta charset="utf-8">
		<title>Attendance System</title>
		<!-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> -->
		<link rel = "icon" href = "img/logo.jpg" type = "image/icon">
		<script src="script.js"></script>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<div class="open">
			<div class="layer"></div>
			<div class="layer"></div>
		</div>
		<section>
			<div class="header">
				<h2 class="logo"><img src="img/logo.jpg"> </h2>
				<ul>
					<li>
						<a href="#" class="active">Branda</a>
					</li>
					<li>
						 <a href="about_us/" >Tentang Kami</a>
					</li>
					<li>
						<a  href="login.php" onclick="myFunction()">Login</a>
					</li>

				</ul>
			</div>
			<div class="bannerText">
				<h2>Attendance</h2><br>
				<h3>System</h3>
				<p>Aplikasi Attendance System ini berfungsi untuk melakuakan absensi siswa dan melakukan  pemberitahuan melalui aplikasi messanger </p>
				
			</div>
			<!-- <div class="bulb"></div> -->
			<img src="img/landing.jpg"  class="bulb">
			<!-- <div class="element1"></div> -->
			<div class="element2"></div>
			<div id="myModal" class="modal">

			  <!-- Modal content -->
			  <div class="modal-content">
			    <span class="close">&times;</span>
					<h3>Tentang Kami</h3>
			    <p>Tim Pengembang Muhamad Ubaidilah</p>
			    <p>NIM J3D217187</p>
			  </div>
			</div>
		</section>
	</body>
	<script src="script.js"></script>

</html>
<?php
// } ?>