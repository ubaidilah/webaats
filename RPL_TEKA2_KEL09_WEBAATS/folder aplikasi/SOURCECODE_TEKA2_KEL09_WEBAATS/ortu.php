<?php 
require_once 'header.php';

?>


     <!-- page content -->
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2>
                 <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="beranda.php">Dashboard</a>
                  </li>
            <li class="breadcrumb-item active">Orangtua Murid</li>
          </ol></h2>
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Orangtua Murid</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                      </li>
                    </ul>
                    <div class="clearfix">
                      

                    </div>

                  </div>
                  <div class="x_content">
                    <center><a href="#add" data-toggle="modal"><button type='button' class='btn btn-success btn-sm'><span class='glyphicon glyphicon-plus' aria-hidden='true'> Tambah ortu </span></button></a></center>
                    
                      <table id="datatable" class="table table-striped table-bordered" style="text-align: center;">
                      <thead>
                    <tr>
                   <th>  No </th>
                    <th>  Id Ortu </th>
                    <th> Nama </th>
                    <th> NIS Anak  </th>
                    <th> JK </th>
                    <th> foto</th>
                    <th>  Action </th>
                    </tr>
                  </thead>
                       
                      <tbody>

                     <?php 
                  
                    $x =1;
                    $sql = "SELECT id_ortu, nama,kd_nis, alamat,no_hp, jk, email,foto FROM orang_tua ";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $id_ortu = $row['id_ortu'];
                            $nis = $row['kd_nis'];
                            $nama = $row['nama'];
                            $alamat = $row['alamat'];
                            $nohp = $row['no_hp'];
                            $email = $row['email'];
                            $jk = $row['jk'];
                            $foto = $row['foto'];

                       echo "
                    <tr>
      
                      <td><center>$x</center></td>
                      <td><center>$id_ortu</center></td>
                      <td><center>$nama</center></td>
                      <th><center>$nis</center></th>
                      <td><center>$jk</center></td>
                      <td><center>$foto</center></td>
                       "; 
                    $x++; ?>
                     <td> <center>
                            <a href="#edit<?php echo $id_ortu;?>" data-toggle="modal"><button type='button' class='btn btn-warning btn-sm gfa-edit '><span class='glyphicon glyphicon-edit' aria-hidden='true'>Ubah</span></button></a>
                            <a href="#delete<?php echo $id_ortu;?>" data-toggle="modal"><button type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-trash' aria-hidden='true'> Hapus</span></button></a>
                     </center>
                    </td>
                  </tr>



  <!--Edit Item Modal -->
        <div id="edit<?php echo $id_ortu; ?>" class="modal fade" role="dialog">
            <form method="post" action="u_ortu.php" >
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Pembaharuan Data ortu</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <!--  // SELECT `id_ortu`, `nama`, `kd_nis`, `alamat`, `no_hp`, `foto`, `jk`, `email` FROM `orang_tua` WHERE 1 -->
                        </div>
                        <div class="modal-body">
          
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">ID Ortu</label>
                                    <input type="number" class="form-control" id="editortu" name="editortu" value="<?php echo $id_ortu; ?>" placeholder="NIS" readonly>
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="editnama">Nama </label>
                               <input type="text" class="form-control" id="editnama" name="editnama" value="<?php echo $nama; ?>" placeholder="Nama ortu" >
                                  </div>
                                   <div class="form-group">
                                    <label class="control-label col-sm-3" for="tkelas">Nis Siswa</label>
			                          <select name="editnis"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
			                   			<option Value = '<?= $nis; ?>' selected  required><?= $nis; ?> </option>
			                                    <?php 
			              
			                          
			                                    $sql4 = "SELECT * FROM siswa ";
			                                    $result3 = $conn->query($sql4);
			                                    while($data = $result3->fetch_assoc()) {
			                                      $nisu = $data["nis"];
			                                      echo '<option value ="'.$nisu.'">'.$nisu.'</option>';
			                                      
			                                    }

			                                    ?>
			               				</select>
			                        </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editnohp">Nohp</label>
                              	<input type="text" class="form-control" id="editnohp" name="editnohp" value="<?php echo $nohp; ?>" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editalamat">Alamat</label>
                                <input type="text" class="form-control" id="editalamat" name="editalamat" value="<?php echo $alamat; ?>" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editemail">Email</label>
                                <input type="text" class="form-control" id="editemail" name="editemail" value="<?php echo $email; ?>" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editjk">Jenis Kelamin</label>
                                <select name="editjk"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
			                   			<option Value = '<?= $jk ?>'   required><?php if ($jk == 'L') 
			                   				echo "Laki - Laki";
			                   			else echo "Perempuan"; ?></option>
			                   			<option Value = 'L'   required>Laki - Laki </option>
			                   			<option Value = 'P'   required>Perempuan</option>
			                                
			               			</select>
                                </div>
                                <div class="form-group">
                                	<img src="">
                              	<input type="hidden" class="form-control"  name="editfoto" value="<?php echo $foto; ?>" >
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit"  name="ubah" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span> Ubah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
                  <!-- delete Unit  Modal-->
     <div id="delete<?php echo $id_ortu; ?>" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form method="post">
                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">
                           <h4 class="modal-title">Delete</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                        </div>

                        <div class="modal-body">
                            <input type="hidden" name="delete_id" value="<?php echo $id_ortu; ?>">
                            <p>
                                <div class="alert alert-danger">Apakah kamu yakin Mau Menghapus <strong><?php echo $nama; ?>?</strong></p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="delete" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YA</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> TIDAK</button>
                            </div>
                        </div>
                </form>
                </div>
            </div>
         
    
                    <?php  } }?>


        </form>
        </div>


                 

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->
                       <!--add Item Modal -->
        <div id="add" class="modal fade" role="dialog">
            <form method="post" action="t_ortu.php">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Tambah Data ortu</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                                  </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">Id Ortu</label>
                                    <input type="number" class="form-control" id="tid" name="tid" aceholder=" ID ortu" >
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="tambahortu">Nama ortu</label>
                               <input type="text" class="form-control" id="tambahortu" name="tambahnama" laceholder="Nama ortu" >
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-3" for="tkelas">Nis  </label>
                                <select name="nis"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
                              <option Value = '' selected  required>Nis Siswa </option>
                                          <?php 
                                
                                          $sql3 = "SELECT * FROM siswa ";
                                          $result3 = $conn->query($sql3);
                                          while($data = $result3->fetch_assoc()) {
                                            $idmpl = $data["nis"];
                                            $mpl = $data["nama_siswa"];
                                            echo '<option value ="'.$idmpl.'">'.$idmpl.'</option>';
                                            
                                          }

                                          ?>
                            </select>
                              </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="tambahalmt">Alamat</label>
                                <input type="text" class="form-control" id="tambahalmt" name="tambahalamat"  >
                                </div>
                                <div class="form-group">
                                  <center>
                        <a  target="_blank" href="https://api.telegram.org/bot511924710:AAEyZqk6DbiLbCx1D0GMsZhI_ccf_ft1EJM/getupdates" rel="noopener noreferrer"><button type='button' class='btn btn-success btn-sm'><span class='glyphicon glyphicon-globe' aria-hidden='true'> Lihat Telegram Id </span></button></a></center>
                                <label class="control-label col-sm-3" for="tambahhp">ID Telegram</label>
                                <input type="text" class="form-control" id="tambahhp" name="tambahnohp" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="tambahhp">Email</label>
                                <input type="email" class="form-control" id="tambahhp" name="tambahemail" >
                                </div>
                                <div class="form-group">
                                   <label class="control-label col-sm-3" for="tambahhp">Jenis Kelamin</label>
                                   <select name="tambahjk"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
                              <option Value = 'L' selected  required>Laki - Laki </option>
                              <option Value = 'P'   required>Perempuan</option>
                                      
                          </select>
                        </div>
                                <div class="form-group">
                                  <img src="">
                                <input type="hidden" class="form-control" id="tambahft" name="tambahfoto" >
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit"  name="tambah" class="btn btn-primary"><span class="glyphicon glyphicon-tambah"></span> tambah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>

<?php 

  if(isset($_POST['delete'])){
                           
                            $delete_id = $_POST['delete_id'];
                            $sql = "DELETE FROM orang_tua WHERE id_ortu ='$delete_id' ";
                            if ($conn->query($sql) === TRUE) {
                                echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Berhasil Dihapus',
                                              type: 'success',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('ortu.php');
                                    } ,3000); 
                                    </script>";
                            } else {
                               echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Gagal Dihapus',
                                              type: 'error',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('ortu.php');
                                    } ,3000); 
                                    </script>";
                            
                            }
                        }
 ?>




<?php
require_once 'footer.php';
?>