<?php 
require_once 'header.php';

?>

     <!-- page content -->
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2>
                 <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="index.php">Dashboard</a>
                  </li>
            <li class="breadcrumb-item active">Siswa</li>
          </ol></h2>
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Siswa</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                      </li>
                    </ul>
                    <div class="clearfix">
                      

                    </div>

                  </div>
                  <div class="x_content">
                    <center><a href="#add" data-toggle="modal"><button type='button' class='btn btn-success btn-sm'><span class='glyphicon glyphicon-plus' aria-hidden='true'> Tambah Siswa </span></button></a></center>
                    
                        <table id="datatable" class="table table-striped table-bordered" style="text-align: center;">
                      <thead>
                    <tr>
                   <th><center>  No </center></th>
                    <th><center>  NIS </center></th>
                    <th> <center>Nama </center></th>
                    <th> <center>Kelas </center></th>
                    <th> <center>JK </center></th>
                    <th> <center>foto</center></th>
                    <th><center>  Action </center></th>
                    </tr>
                  </thead>
                       
                      <tbody>

                     <?php 
                 
                    $x =1;
                    $sql = "SELECT nis, nama_siswa,kd_kelas, kd_ortu, kelas.nama_kelas, orang_tua.nama, siswa.tgl_lahir, siswa.alamat, siswa.no_hp, siswa.email, siswa.jk, siswa.foto, siswa.akses FROM siswa LEFT JOIN kelas ON (siswa.kd_kelas = kelas.id_kelas ) 
                    LEFT JOIN orang_tua ON (siswa.kd_ortu = orang_tua.id_ortu) ";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $nis = $row['nis'];
                            $nama = $row['nama_siswa'];
                            $kelas = $row['kd_kelas'];
                            $ortu = $row['kd_ortu'];
                            $tgl_lahir = $row['tgl_lahir'];
                            $alamat = $row['alamat'];
                            $nohp = $row['no_hp'];
                            $email = $row['email'];
                            $jk = $row['jk'];
                            $foto = $row['foto'];
                            $namakelas = $row['nama_kelas'];
                            $namaortu = $row['nama'];

                       echo "
                    <tr>
      
                      <td><center>$x</center></td>
                      <th><center>$nis</center></th>
                      <td><center>$nama</center></td>
                      <td><center>$namakelas</center></td>
                      <td><center>$jk</center></td>
                      <td ><img src='$lokpp$foto' style='width:10%'' ></td>
                       "; 
                    $x++; ?>
                     <td> <center>
                            <a href="#edit<?php echo $nis;?>" data-toggle="modal"><button type='button' class='btn btn-warning btn-sm gfa-edit '><span class='glyphicon glyphicon-edit' aria-hidden='true'>Ubah</span></button></a>
                            <a href="#delete<?php echo $nis;?>" data-toggle="modal"><button type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-trash' aria-hidden='true'> Hapus</span></button></a>
                     </center>
                    </td>
                  </tr>


   <!--add Item Modal -->
        <div id="add" class="modal fade" role="dialog">
            <form method="post" action="t_siswa.php">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Tambah Data Siswa</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                                  </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">NIS</label>
                                    <input type="number" class="form-control" id="tambahnis" name="tambahnis" aceholder=" NIS Siswa" >
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="tambahsiswa">Nama Siswa</label>
                               <input type="text" class="form-control" id="tambahsiswa" name="tambahnama" laceholder="Nama Siswa" >
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-sm-3" for="tkelas">Kelas</label>
			                          <select name="tambahkelas"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
			                   			<option Value = '' selected  required>Pilih Kelas </option>
			                                    <?php 
			                          
			                                    $sql3 = "SELECT * FROM kelas ";
			                                    $result3 = $conn->query($sql3);
			                                    while($data = $result3->fetch_assoc()) {
			                                      $idkls = $data["id_kelas"];
			                                      $nmkls = $data["nama_kelas"];
			                                      echo '<option value ="'.$idkls.'">'.$nmkls.'</option>';
			                                      
			                                    }

			                                    ?>
			               				</select>
			                        </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="tambahortu">Orang Tua</label>
                               <input type="text" class="form-control" id="tambahortu" name="tambahortu" >
                                  </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="tambahtgllahir">Tanggal Lahir</label>
                              	<input type="date" class="form-control" id="tambahtgllahir" name="tambahtgl_lahir"  >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="tambahalmt">Alamat</label>
                              	<input type="text" class="form-control" id="tambahalmt" name="tambahalamat"  >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="tambahhp">No Hp</label>
                              	<input type="text" class="form-control" id="tambahhp" name="tambahnohp" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="tambaheml">Email</label>
                              	<input type="text" class="form-control" id="tambaheml" name="tambahemail" >
                                </div>
                                <div class="form-group">
                               		 <select name="tambahjk"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
			                   			<option Value = 'L' selected  required>Laki - Laki </option>
			                   			<option Value = 'P'   required>Perempuan</option>
			                                
			               			</select>
			               		</div>
                                <div class="form-group">
                                	<img src="">
                              	<input type="hidden" class="form-control" id="tambahft" name="tambahfoto" >
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit"  name="tambah" class="btn btn-primary"><span class="glyphicon glyphicon-tambah"></span> tambah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>

  <!--Edit Item Modal -->
        <div id="edit<?php echo $nis; ?>" class="modal fade" role="dialog">
            <form method="post" action="u_siswa.php" >
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                             <h4 class="modal-title">Pembaharuan Data Siswa</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                         
                        </div>
                        <div class="modal-body">
          
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="id">NIS</label>
                                    <input type="number" class="form-control" id="editnis" name="editnis" value="<?php echo $nis; ?>" placeholder="NIS" readonly>
                                  </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="editnama">Nama Siswa</label>
                               <input type="text" class="form-control" id="editnama" name="editnama" value="<?php echo $nama; ?>" placeholder="Nama Siswa" >
                                  </div>
                                   <div class="form-group">
                                    <label class="control-label col-sm-3" for="tkelas">Kelas</label>
			                          <select name="editkelas"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
			                   			<option Value = '<?= $kelas; ?>' selected  required><?= $namakelas; ?> </option>
			                                    <?php 
			                          
			                                    $sql3 = "SELECT * FROM kelas ";
			                                    $result3 = $conn->query($sql3);
			                                    while($data = $result3->fetch_assoc()) {
			                                      $idkls = $data["id_kelas"];
			                                      $nmkls = $data["nama_kelas"];
			                                      echo '<option value ="'.$idkls.'">'.$nmkls.'</option>';
			                                      
			                                    }

			                                    ?>
			               				</select>
			                        </div>
                                  <div class="form-group">
                                <label class="control-label col-sm-3" for="editortu">Orang Tua</label>
                               <input type="text" class="form-control" id="editortu" name="editortu" value="<?php echo $ortu; ?>" >
                                  </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="edittgl_lahir">Tanggal Lahir</label>
                              	<input type="text" class="form-control" id="edittgl_lahir" id="single_cal3" name="edittgl_lahir" value="<?php echo $tgl_lahir; ?>" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editalamat">Alamat</label>
                              	<input type="text" class="form-control" id="editalamat" name="editalamat" value="<?php echo $alamat; ?>" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editnohp">No Hp</label>
                              	<input type="text" class="form-control" id="editnohp" name="editnohp" value="<?php echo $nohp; ?>" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editemail">Email</label>
                              	<input type="text" class="form-control" id="editemail" name="editemail" value="<?php echo $email; ?>" >
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3" for="editjk">Jenis Kelamin</label>
                                <select name="editjk"  style="width:100%" class="form-control selectpicker" data-live-search="true"  required >
			                   			<option Value = '<?= $jk ?>'   required><?php if ($jk == 'L') 
			                   				echo "Laki - Laki";
			                   			else echo "Perempuan" ?></option>
			                   			<option Value = 'L'   required>Laki - Laki </option>
			                   			<option Value = 'P'   required>Perempuan</option>
			                                
			               			</select>
                                </div>
                                <div class="form-group">
                                	<img src="">
                              	<input type="hidden" class="form-control" id="edithead" name="editfoto" value="<?php echo $foto; ?>" >
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit"  name="ubah" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span> Ubah</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Batal</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
                  <!-- delete Unit  Modal-->
     <div id="delete<?php echo $nis; ?>" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form method="post">
                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="modal-header">
                           <h4 class="modal-title">Delete</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                           
                        </div>

                        <div class="modal-body">
                            <input type="hidden" name="delete_id" value="<?php echo $nis; ?>">
                            <p>
                                <div class="alert alert-danger">Apakah kamu yakin Mau Menghapus <strong><?php echo $nama; ?>?</strong></p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" name="delete" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YA</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> TIDAK</button>
                            </div>
                        </div>
                </form>
                </div>
            </div>
         
    
                    <?php  } }?>

                 

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->


<?php 

  if(isset($_POST['delete'])){
                           
                            $delete_id = $_POST['delete_id'];
                            $sql = "DELETE FROM siswa WHERE nis='$delete_id' ";
                            if ($conn->query($sql) === TRUE) {
                                echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Berhasil Dihapus',
                                              type: 'success',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('siswa.php');
                                    } ,3000); 
                                    </script>";
                            } else {
                               echo "
                                    <script type='text/javascript'>
                                    setTimeout(function () { 
                                    
                                      swal({
                                              title: 'Data Gagal Dihapus',
                                              type: 'error',
                                              timer: 3000,
                                              showConfirmButton: true
                                          });   
                                    },10);  
                                    window.setTimeout(function(){ 
                                      window.location.replace('siswa.php');
                                    } ,3000); 
                                    </script>";
                            
                            }
                        }
 ?>


<?php
require_once 'footer.php';
?>