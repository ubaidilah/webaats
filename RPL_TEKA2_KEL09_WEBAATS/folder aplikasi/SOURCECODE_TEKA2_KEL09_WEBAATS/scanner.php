<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Scan</title>
        <script type="text/javascript" src="js/qrcodelib.js"></script>
        <script type="text/javascript" src="js/webcodecamjs.js"></script>
    </head>
    <body>
    	<h3>Scan QR</h3>

      <?php 
        // $_POST["username"]
      ?>
      
                   <section class="container">
              <select id="scanner-cams" hidden></select>
              <canvas id="scanner-canvas"></canvas>
              <form method="post" action="datakehadiran.php">
                  <input type="hidden" name="jdl" value="<?=$_POST['idj'];?>">
                  <input type="hidden" name="nipj" value="<?=$_POST['nipj'];?>">
                  <input  type="submit" name="nis" id="scanner-result" class="ubah" required> 
              </form>
              <a href="beranda.php"><button>Kembali</button></a>
      
            <!-- <textarea id="scanner-result"></textarea> -->
              
              <section id="scanner-buttons"></section>
            </section>
            <script src="./js/qrcodelib.js"></script>
            <script src="./js/webcodecamjs.js"></script>
            <script>
              const canvasEl = document.getElementById('scanner-canvas')
              
              const resultEl = document.getElementById('scanner-result')

              let decoder = new WebCodeCamJS(canvasEl)
              const options = {
                autoBrightnessValue: 100,
                beep: 'audio/beep.mp3',
                resultFunction: function (result) {
                  resultEl.innerHTML = result.code
                  resultEl.value = result.code
                }
              }

              decoder.buildSelectMenu('#scanner-cams', 'environment|back').init(options)
              decoder.play()
            </script>
            
    </body>
</html>