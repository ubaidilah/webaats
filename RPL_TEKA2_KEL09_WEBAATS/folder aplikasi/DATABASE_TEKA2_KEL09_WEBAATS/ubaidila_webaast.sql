-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 23, 2019 at 09:05 AM
-- Server version: 5.5.64-MariaDB-cll-lve
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ubaidila_webaast`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `jk` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chat_bot`
--

CREATE TABLE `chat_bot` (
  `id_bot` int(11) NOT NULL,
  `token` varchar(256) NOT NULL,
  `nama_bot` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_bot`
--

INSERT INTO `chat_bot` (`id_bot`, `token`, `nama_bot`) VALUES
(1, '511924710:AAEyZqk6DbiLbCx1D0GMsZhI_ccf_ft1EJM', 'Attendance');

-- --------------------------------------------------------

--
-- Table structure for table `daftar_anak`
--

CREATE TABLE `daftar_anak` (
  `no` int(11) NOT NULL,
  `nis` varchar(12) NOT NULL,
  `id_ortu` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daftar_anak`
--

INSERT INTO `daftar_anak` (`no`, `nis`, `id_ortu`) VALUES
(1, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `nip` varchar(12) NOT NULL,
  `nama_guru` varchar(175) NOT NULL,
  `mapel` varchar(100) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `jk` varchar(10) NOT NULL,
  `email` varchar(150) NOT NULL,
  `foto` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`nip`, `nama_guru`, `mapel`, `alamat`, `jk`, `email`, `foto`) VALUES
('1', 'Ubaidilah', '1', 'bogor', 'L', 'ubay@ubay.com', 'default.jpg'),
('3217187', 'ubay', '1', 'Bogor', 'L', 'ubay@gmail.com', 'default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` varchar(12) NOT NULL,
  `id_mapel` varchar(12) NOT NULL,
  `id_kelas` varchar(15) NOT NULL,
  `waktu_masuk` time NOT NULL,
  `waktu_keluar` time NOT NULL,
  `nip` varchar(12) NOT NULL,
  `hari` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `id_mapel`, `id_kelas`, `waktu_masuk`, `waktu_keluar`, `nip`, `hari`) VALUES
('1', '1', '1', '11:00:00', '18:00:00', '1', 'Rabu');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` varchar(25) NOT NULL,
  `nama_jurusan` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
('00002', 'IPS'),
('1', 'IPA');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` varchar(12) NOT NULL,
  `nama_kelas` varchar(35) NOT NULL,
  `nip_walikelas` varchar(12) NOT NULL,
  `id_jurusan` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `nama_kelas`, `nip_walikelas`, `id_jurusan`) VALUES
('1', 'X IPA 1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` varchar(12) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `nama_mapel`) VALUES
('00002', 'B.inggris'),
('1', 'Matematika');

-- --------------------------------------------------------

--
-- Table structure for table `orang_tua`
--

CREATE TABLE `orang_tua` (
  `id_ortu` int(11) NOT NULL,
  `nama` varchar(75) NOT NULL,
  `kd_nis` varchar(12) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `jk` char(2) NOT NULL,
  `email` varchar(75) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orang_tua`
--

INSERT INTO `orang_tua` (`id_ortu`, `nama`, `kd_nis`, `alamat`, `no_hp`, `foto`, `jk`, `email`) VALUES
(1, 'Kusum Aja', '1', 'Bogor', '609650958', 'default.jpg', 'L', 'kusum@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `no_pesan` int(11) NOT NULL,
  `nis` varchar(12) NOT NULL,
  `kontak_ortu` varchar(25) NOT NULL,
  `isi_pesan` text NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nip` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `presensi`
--

CREATE TABLE `presensi` (
  `no` int(11) NOT NULL,
  `id_mapel` varchar(12) NOT NULL,
  `waktu_masuk` time NOT NULL,
  `status` varchar(150) NOT NULL,
  `ket` varchar(250) NOT NULL,
  `nis` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `presensi`
--

INSERT INTO `presensi` (`no`, `id_mapel`, `waktu_masuk`, `status`, `ket`, `nis`) VALUES
(1, '1', '00:00:00', 'Terlambat', '', ''),
(2, '1', '13:40:41', 'Terlambat', '', ''),
(3, '1', '11:34:34', 'Terlambat', '', ''),
(4, '1', '13:56:27', 'Terlambat', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `nis` varchar(15) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `kd_kelas` varchar(3) NOT NULL,
  `kd_ortu` int(11) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `email` varchar(100) NOT NULL,
  `jk` char(2) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `akses` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`nis`, `nama_siswa`, `kd_kelas`, `kd_ortu`, `tgl_lahir`, `alamat`, `no_hp`, `email`, `jk`, `foto`, `akses`) VALUES
('1', 'Daffaaaa', '1', 1, '1999-10-09', 'KP.apa aja ', '081234', 'daffa@dafa.com', 'L', 'default.jpg', 'siswa'),
('11', 'yandra', '1', 0, '2000-11-19', 'bekasi', '08388812345', 'yandra@gmail.com', 'L', 'default.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(150) NOT NULL,
  `password` varchar(256) NOT NULL,
  `id_ortu` varchar(12) DEFAULT NULL,
  `nis` varchar(12) DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL,
  `nip` varchar(12) DEFAULT NULL,
  `akses` varchar(35) NOT NULL,
  `waktu_daftar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `id_ortu`, `nis`, `id_admin`, `nip`, `akses`, `waktu_daftar`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, '1', NULL, 'admin', '2019-11-07 18:10:16'),
('guru', '77e69c137812518e359196bb2f5e9bb9', NULL, NULL, NULL, '1', 'guru', '2019-11-12 23:54:10'),
('hanifah', '$2y$10$RkE8uOUz4CIvAPLv/XcqS.k0IIWj2XEGn//6TfkvvWKTeXYOs5djm', NULL, NULL, NULL, NULL, 'siswa', '2019-11-06 16:26:38'),
('nida', '$2y$10$8.3QuR5wL9aWx/o1.nKnrO/zsZ4yIvTP1MKDzsraSlBjPVDveqVQG', NULL, NULL, NULL, NULL, 'admin', '0000-00-00 00:00:00'),
('ortu', '469eb28221c8e6d092ddafacb87799bf', NULL, NULL, NULL, NULL, 'ortu', '2019-11-13 16:37:27'),
('siswa', 'bcd724d15cde8c47650fda962968f102', '1', '1', NULL, NULL, 'siswa', '2019-11-20 15:29:14'),
('ubaidilah', '$2y$10$AKR/wogHhygYEZyRqVM3pOvgoVPldJdkbzar1XJFiFRkBJKrTLkFC', NULL, NULL, NULL, NULL, 'admin', '2019-11-05 17:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `chat_bot`
--
ALTER TABLE `chat_bot`
  ADD PRIMARY KEY (`id_bot`);

--
-- Indexes for table `daftar_anak`
--
ALTER TABLE `daftar_anak`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `orang_tua`
--
ALTER TABLE `orang_tua`
  ADD PRIMARY KEY (`id_ortu`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`no_pesan`);

--
-- Indexes for table `presensi`
--
ALTER TABLE `presensi`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_bot`
--
ALTER TABLE `chat_bot`
  MODIFY `id_bot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `daftar_anak`
--
ALTER TABLE `daftar_anak`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orang_tua`
--
ALTER TABLE `orang_tua`
  MODIFY `id_ortu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `no_pesan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `presensi`
--
ALTER TABLE `presensi`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
