<?php
require_once 'akses.php'; 
require_once 'koneksi.php'; 
$sqlusr = "SELECT * FROM user WHERE username = '".$_SESSION['admin']."'";
      $resultusr = $conn->query($sqlusr);
      $rowuser=mysqli_fetch_object($resultusr);
      if ($_SESSION['level'] == 'admin'){
      $detail_user = $rowuser->id_admin;} 
      elseif ($_SESSION['level'] == 'guru') {
        $detail_user = $rowuser->nip;
      }elseif ($_SESSION['level'] == 'siswa') {
        $detail_user = $rowuser->nis;
      }elseif ($_SESSION['level'] == 'ortu') {
        $detail_user = $rowuser->id_ortu;
      }

       $sqldtkls = "SELECT * FROM siswa WHERE nis = '$detail_user'";
      $resultsisk = $conn->query($sqldtkls);
      $rowdatkel=mysqli_fetch_object($resultsisk);
      $siswa_kelas = $rowdatkel->kd_kelas;

     
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Jadwal Pelajaran</title>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css">
</head>
<body>
<table id="example" class="display" style="width:100%">
        <thead>
				<tr>
                          <th>NO</th>
                          <th>Jam</th>
                          <th>Senin</th>
                          <th>Selasa</th>
                          <th>Rabu</th>
                          <th>Kamis</th>
                          <th>Jumat</th>
                          <th>Sabtu</th>
                </tr>
        </thead>
        <tbody>
           <?php $y=1;
                        $datajdwsis = "SELECT id_jadwal, id_kelas, waktu_masuk, waktu_keluar, guru.nip, hari,mapel.nama_mapel,mapel.id_mapel, guru.nama_guru FROM jadwal 
                  LEFT JOIN mapel ON (jadwal.id_mapel = mapel.id_mapel )
                  LEFT JOIN guru ON (jadwal.nip = guru.nip ) WHERE id_kelas = '$siswa_kelas' ORDER BY jadwal.waktu_masuk ASC
                    "; 
                   

                   $resultjdwlsis =$conn->query($datajdwsis);
                      if ($resultjdwlsis->num_rows > 0) {
                        while ($rowj = $resultjdwlsis->fetch_assoc()) {
                          $id = $rowj['id_jadwal'];
                          $kdmapel = $rowj['id_mapel'];
                          $mapel = $rowj['nama_mapel'];
                          $guru = $rowj['nama_guru'];
                          $nip = $rowj['nip'];
                          $kdkelas = $rowj['id_kelas'];
                          $wmasuk = $rowj['waktu_masuk'];
                          $wkeluar = $rowj['waktu_keluar'];
                          $nipj = $rowj['nip'];
                          $hr = $rowj['hari']; 

                       echo "
                    <tr>
      
                      <td><center>$y</center></td>
                      <td><center>$wmasuk - $wkeluar</center></td>
                      
                      
                      

                       "; 
                       if ('Senin' == $hr){
                           echo "<td><center>$mapel</center></td>";
                       }else{
                        echo "<td><center></center></td>";
                       }
                      if ('Selasa' == $hr){
                           echo "<td><center>$mapel</center></td>";
                       }else{
                        echo "<td><center></center></td>";
                       }
                       

                       if ('Rabu' == $hr){
                           echo "<td><center>$mapel</center></td>";
                       }else{
                        echo "<td><center></center></td>";
                       }
                       if ('Kamis' == $hr){
                           echo "<td><center>$mapel</center></td>";
                       }else{
                        echo "<td><center></center></td>";
                       }
                       if ('Jumat' == $hr){
                           echo "<td><center>$mapel</center></td>";
                       }else{
                        echo "<td><center></center></td>";
                       }
                       if ('Sabtu' == $hr){
                           echo "<td><center>$mapel</center></td>";
                       }else{
                        echo "<td><center></center></td>";
                       }

                    $y++; }}?>
        </tbody>
        <!-- <tfoot>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
            </tr>
        </tfoot> -->
    </table>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'pdfHtml5'
        ]
    } );
} );
</script>
</html>
